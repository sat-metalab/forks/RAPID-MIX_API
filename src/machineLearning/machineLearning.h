/**
 * @file machineLearning.h
 * @author Michael Zbyszynski on 10 Jan 2016
 * @copyright
 * Copyright © 2017 Goldsmiths. All rights reserved.
 *
 * @ingroup machinelearning
 */

#ifndef machineLearning_h
#define machineLearning_h

#include <vector>
#include "../rapidmix.h"

////////// Include all of the machine learning algorithms here
#include "classification.h"
#include "regression.h"
#include "seriesClassification.h"
#include "./rapidXMM/rapidXMM.h"
#include "./rapidGVF/rapidGVF.h"

namespace rapidmix {
    
    // forward declaration
    class trainingData;
    
    /** @brief A generic ouptut struct to fit all kinds of models */
    typedef struct runResults_t {
        std::vector<double> likelihoods;
        std::vector<double> regression;
        std::vector<double> progressions;
        std::string likeliest;
    } runResults;
    
    /**
     * @brief Host class for machine learning algorithms
     */
    template <typename MachineLearningModule>
    class machineLearning : public MachineLearningModule {
    public:
        
        //* Constructors */
        machineLearning() : MachineLearningModule() {};
        
        template<class T>
        machineLearning(T type) : MachineLearningModule(type) {};
        
        /**
         * @brief This function becomes specialized in the implementation
         */
        bool train(const trainingData &newTrainingData);
        
        //* this function is not being specialized
        std::vector<double> run(const std::vector<double> &inputVector) {
            return MachineLearningModule::run(inputVector);
        }
        
        // This is a hack while I think about how to do this. -MZ //
        std::string run(const std::vector<double> &inputVector, const std::string &label);
        
        //* This is the one I'm using for DTW */
        std::string run(const std::vector<std::vector<double> > &inputSeries);
        
        bool reset() {
            return MachineLearningModule::reset();
        }
        
    private:
        MachineLearningModule module;
        
        //this holds string labels
        std::vector<std::string> labels; //FIXME: This probably should be pushed down into rapidLib?
        std::string getLabel(int value);
        
    };
    
    ////////// typedefs for calling different algorithms
    
    ///// RapidLib
    
    /** @brief static classification using KNN from RapidLib */
    typedef machineLearning<classification> staticClassification;
    
    /** @brief static regression using Neural Networks from RapidLib */
    typedef machineLearning<regression> staticRegression;
    
    /** @brief temporal classification using Dynamic Time Warping from RapidLib */
    typedef machineLearning<seriesClassification> dtwTemporalClassification;
    
    ///// XMM
    
    /** @brief configuration for XMM based algorithms */
    typedef xmmToolConfig xmmConfig;
    
    /** @brief static classification using Gaussian Mixture Models from XMM */
    typedef machineLearning<rapidXmmGmm> xmmStaticClassification;
    
    /** @brief static regression using Gaussian Mixture Models from XMM */
    typedef machineLearning<rapidXmmGmr> xmmStaticRegression;
    
    /** @brief temporal classification using Hierarchical Hidden Markov Models from XMM */
    typedef machineLearning<rapidXmmHmm> xmmTemporalClassification;
    
    /** @brief temporal regression using Hierarchical Hidden Markov Models from XMM */
    typedef machineLearning<rapidXmmHmr> xmmTemporalRegression;
    
    ///// GVF
    
    /** @brief temporal variation estimation using GVF library */
    typedef machineLearning<rapidGVF> gvfTemporalVariation;
    
    
}

#endif
