/**
 * @file signalProcessing.h
 * @author Michael Zbyszynski
 * @date 6 Feb 2017
 * @copyright
 * Copyright © 2017 Goldsmiths. All rights reserved.
 *
 * @ingroup signalprocessing
 */

#ifndef signalProcessing_h
#define signalProcessing_h

#include "rapidmix.h"
#include "maximilian.h"
#include "maxim.h"
#include "rapidStream.h"
#include "./rapidPiPoTools/rapidPiPoTools.h"

namespace rapidmix {

    /*
     * Wrapper for signalProcessing modules, currently a collection of typedefs
     */

    typedef maxiFFT FFT;
    typedef maxiMFCC MFCC;
    typedef rapidStream<double> rapidStream;

    typedef signalProcessingHost signalProcessingHost;
    typedef signalAttributes signalAttributes;
}

#endif
