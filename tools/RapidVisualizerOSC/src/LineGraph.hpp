//
//  LineGraph.hpp
//  RapidVisualizerOSC
//
//  Created by James on 09/11/2017.
//
//

#ifndef LineGraph_h
#define LineGraph_h

#include <stdio.h>
#include "RealTimeGraph.hpp"

class LineGraph : public RealTimeGraph
{
public:
    LineGraph ( GraphState* state ) : RealTimeGraph (state)
    {
        //
    }
    ~LineGraph ( void )
    {
        //
    }
    void updateRep ( void )
    {
        //
    }
    void drawSubGraph ( std::string subLabel, DataContainer<std::vector<double>>& data, ofRectangle subLabelRect )
    {
        double
        minIn = 0,
        minOut = 0,
        maxOut = -subLabelRect.height,
        startPosY = subLabelRect.height,
        pointDistance = subLabelRect.width/data.labelData.size(),
        separation = pointDistance/2;
        //halfSeparation = separation/2;
        bool drawZeroSep = false;
        
        if (data.minValue < 0)
        { // Add negative part
            startPosY = subLabelRect.height/2;
            minIn = -data.maxValue;
            minOut = subLabelRect.height/2;
            maxOut /= 2;
            drawZeroSep = true;
        }
        
        ofBeginShape();
        ofFill();
        ofVertex(subLabelRect.x, subLabelRect.y + startPosY);
        double output = mapVals(data.labelData[0], minIn, data.maxValue, minOut, maxOut );
        ofVertex(subLabelRect.x, subLabelRect.y + startPosY + output);
        for (uint32_t i = 0; i < data.labelData.size(); ++i)
        {
            output = mapVals(data.labelData[i], minIn, data.maxValue, minOut, maxOut );
            ofSetColor (graphColor);
            
            ofVertex(subLabelRect.x + pointDistance * i + separation, subLabelRect.y + startPosY + output);
            //ofDrawRectangle(subLabelRect.x + barSize * i + halfSeparation, subLabelRect.y + startPosY, barSize - separation, output );
            
        }
        output = mapVals(data.labelData[data.labelData.size()-1], minIn, data.maxValue, minOut, maxOut );
        ofVertex(subLabelRect.x + subLabelRect.width, subLabelRect.y + startPosY + output);
        ofVertex(subLabelRect.x + subLabelRect.width, subLabelRect.y + startPosY);
        ofEndShape();
        
        if (drawZeroSep)
        {
            ofSetLineWidth(1.25);
            ofSetColor (175,150,150);
            ofDrawLine(subLabelRect.x, subLabelRect.y + startPosY,
                       subLabelRect.x + subLabelRect.width, subLabelRect.y + startPosY);
        }
    }
    void update ( void )
    {
        //
    }
};


#endif /* LineGraph_h */
