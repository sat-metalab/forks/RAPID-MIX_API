//
//  RapidVisualizer.cpp
//  RapidVisualizerOSC
//
//  Created by James on 09/11/2017.
//
//

#include "RapidVisualizer.hpp"

RapidVisualizer::RapidVisualizer ( void )
{
    uint32 initialHistorySize = 256;
    minimumGui.setup();
    expandedGui.setBackgroundColor(ofColor(0,0,0,127));
    expandedGui.setup();
    
    historySizeSlider.addListener(this, &RapidVisualizer::historySizeSliderChanged);
    
    auto gNameRef = expandButton.setup(graphState.label, false);
    expandButton.setBackgroundColor(ofColor(0, 0, 0, 127));
    minimumGui.add(gNameRef);

    expandedGui.add(graphTypesLabel.setup("Graph Type",""));
    expandedGui.add(viewTypes[0].setup("Bar Chart", false));
    expandedGui.add(viewTypes[1].setup("Line Graph", false));
    expandedGui.add(viewTypes[2].setup("Line Graph History", false));
    expandedGui.add(viewTypes[3].setup("Scope", false));
    expandedGui.add(prefLabel.setup("Preferences",""));
    expandedGui.add(splitArrayIntoGraphs.setup("Split Multiple Input", false));
    expandedGui.add(historySizeSlider.setup("History Size", initialHistorySize, 4, 4096));
    
    setHistory(initialHistorySize);
    setLayout (Graph::GRAPHS_STACKED);
    setSplitEachArgument (false);
    setRect (ofRectangle(0,0,200,200));
}

RapidVisualizer::~RapidVisualizer ( void )
{
    if (currentGraph)
    {
        delete currentGraph;
    }
}

void RapidVisualizer::setup ( std::string label, graphTypeT graphType, Graph::graphLayout layout, bool splitEachArgument, ofRectangle positionAndSize )
{
    
    setLabel (label);
    setLayout (layout);
    setSplitEachArgument (splitEachArgument);
    setRect (positionAndSize);
    setGraphType (graphType); // FIXME: Check if successfully initialized
}

void RapidVisualizer::setup ( std::string label, graphTypeT graphType, uint32_t historySize, Graph::graphLayout layout, bool splitEachArgument, ofRectangle positionAndSize )
{
    setLabel (label);
    setLayout (layout);
    setSplitEachArgument (splitEachArgument);
    setRect (positionAndSize);
    setGraphType (graphType, historySize); // FIXME: Check if successfully initialized
}

void RapidVisualizer::setLabel ( std::string label )
{
    graphState.label = label;
    expandButton.setName(label);
    if (currentGraph)
        currentGraph->updateRep();
}

void RapidVisualizer::setGraphType ( graphTypeT graphType )
{
    if (currentGraphType != graphType)
    {
        if (currentGraph)
        {
            // TODO: add lock for when doing this, or have all types loaded?
            delete currentGraph;
        }
        switch (graphType) {
            case BAR_CHART:
                currentGraph = new BarChart ( &graphState );
                graphState.hasHistory = false;
                break;
            case LINE_GRAPH:
                currentGraph = new LineGraph ( &graphState );
                graphState.hasHistory = false;
                break;
            case LINE_GRAPH_WITH_HISTORY:
                currentGraph = new LineGraphHistory ( &graphState );
                graphState.hasHistory = true;
                break;
            case SCOPE:
                currentGraph = new ScopeWithHistory ( &graphState );
                graphState.hasHistory = true;
                break;
            default:
                break;
        }
        currentGraphType = graphType;
    }
}

void RapidVisualizer::setGraphType ( graphTypeT graphType, uint32_t historySize )
{
    setHistory(historySize);
    setGraphType(graphType);
}

void RapidVisualizer::setLayout ( Graph::graphLayout layout )
{
    graphState.layout = layout;
    if (currentGraph)
        currentGraph->updateRep();
}

void RapidVisualizer::setSplitEachArgument ( bool splitEachArgument )
{
    this->splitEachArgument = splitEachArgument;
    splitArrayIntoGraphs = splitEachArgument;
}

void RapidVisualizer::setPos ( ofVec2f pos )
{
    graphState.positionAndSize.x = pos.x;
    graphState.positionAndSize.y = pos.y;
    if (currentGraph)
        currentGraph->updateRep();
}

void RapidVisualizer::setSize ( ofVec2f size )
{
    graphState.positionAndSize.width = size.x;
    graphState.positionAndSize.height = size.y;
    if (currentGraph)
        currentGraph->updateRep();
}

void RapidVisualizer::setRect ( ofRectangle positionAndSize )
{
    graphState.positionAndSize = positionAndSize;
    if (currentGraph)
        currentGraph->updateRep();
}

void RapidVisualizer::setHistory( uint32_t historySize )
{
    graphState.historySize = historySize;
    historySizeSlider = historySize;
    if (currentGraph)
        currentGraph->updateRep();
}

void RapidVisualizer::historySizeSliderChanged (int32_t &val)
{
    setHistory(val);
}

void RapidVisualizer::addData ( std::string subLabel, std::vector<double>& data )
{
    if (currentGraph)
    {
        if (splitEachArgument && data.size() > 1)
        {
            int16_t currentIndex = 0;
            std::vector<double> splitData;
            for (double d : data)
            {
                splitData = {d};
                currentGraph->addData (subLabel + ofToString(currentIndex), splitData);
                ++currentIndex;
            }
        }
        else
            currentGraph->addData (subLabel, data);
    }
}

void RapidVisualizer::update ( void )
{
    if (currentGraph)
    {
        currentGraph->update();
    }
}

void RapidVisualizer::drawMenu ( ofRectangle mainPosAndSize )
{
    minimumGui.setPosition(graphState.positionAndSize.x, graphState.positionAndSize.y);
    minimumGui.draw();
    if (menuActive)
    {
        double expandedGuiXPos = graphState.positionAndSize.x + minimumGui.getWidth();
        double expandedGuiYPos = graphState.positionAndSize.y;
        double mainYPlusHeight = mainPosAndSize.y + mainPosAndSize.height;
        double check = expandedGuiYPos + expandedGui.getHeight() - mainYPlusHeight;
        
        if (check > 0)
        {
            expandedGuiYPos -= check;
        }
        
        expandedGui.setPosition(expandedGuiXPos, expandedGuiYPos);
        expandedGui.draw();
    }
}

void RapidVisualizer::draw ( void )
{
    
    if (currentGraph)
    {
        currentGraph->draw();
    }
    //drawMenu();
    menuActive = expandButton;
    if (menuActive)
    {
        bool noneActive = true;
        for (uint8_t i = 0; i < NUMVIEWTYPES; ++i)
        {
            if (viewTypes[i] && !oldTypeToggles[i])
            {
                std::fill(viewTypes, viewTypes + NUMVIEWTYPES, false);
                viewTypes[i] = true;
                setGraphType(static_cast<graphTypeT>(i));
            }
            if (viewTypes[i])
                noneActive = false;
            oldTypeToggles[i] = viewTypes[i];
        }
        if (noneActive && currentGraphType != NOT_INITIALIZED)
        {
            viewTypes[currentGraphType] = true;
        }
        if (splitEachArgument != splitArrayIntoGraphs)
        {
            // Dirty, solution for now
            if (currentGraph)
                currentGraph->reset();
            splitEachArgument = splitArrayIntoGraphs;
        }
    }
}

uint32_t RapidVisualizer::getNumGraphs ( void ) const
{
    if (currentGraph)
    {
        return currentGraph->getNumSubGraphs();
    } else {
        return 0;
    }
}

#undef NUMVIEWTYPES
