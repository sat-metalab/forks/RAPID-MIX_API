//
//  RapidVisualizer.hpp
//  RapidVisualizerOSC
//
//  Created by James on 09/11/2017.
//
//

#ifndef RapidVisualizer_hpp
#define RapidVisualizer_hpp

#include <stdio.h>
#include <map>
#include "ofMain.h"
#include "Graph.hpp"
#include "BarChart.hpp"
#include "LineGraph.hpp"
#include "LineGraphHistory.hpp"
#include "ScopeWithHistory.hpp"

#define NUMVIEWTYPES 4

// TODO add historySize slider and init with default 256 or so

class RapidVisualizer {
public:
    enum graphTypeT {
        BAR_CHART,
        LINE_GRAPH,
        LINE_GRAPH_WITH_HISTORY,
        SCOPE,
        NOT_INITIALIZED
    };
    
    RapidVisualizer ( void );
    ~RapidVisualizer ( void );
    
    void setup ( std::string label, graphTypeT graphType, Graph::graphLayout layout, bool splitEachArgument, ofRectangle positionAndSize );
    void setup ( std::string label, graphTypeT graphType, uint32_t historySize, Graph::graphLayout layout, bool splitEachArgument, ofRectangle positionAndSize );
    
    void setLabel ( std::string label );
    void setGraphType ( graphTypeT graphType );
    void setGraphType ( graphTypeT graphType, uint32_t historySize );
    void setLayout ( Graph::graphLayout layout );
    void setSplitEachArgument ( bool splitEachArgument );
    void setPos ( ofVec2f pos );
    void setSize ( ofVec2f size );
    void setRect ( ofRectangle rect );
    void setHistory ( uint32_t historySize );
    void historySizeSliderChanged (int32_t &val);
    
    void addData ( std::string subLabel, std::vector<double>& data );
    void update ( void );
    void drawMenu ( ofRectangle mainPosAndSize );
    void draw ( void );
    
    uint32_t getNumGraphs ( void ) const;
    
private:
    graphTypeT currentGraphType = NOT_INITIALIZED;
    Graph::GraphState graphState;
    Graph* currentGraph = nullptr;
    bool splitEachArgument = false;
    bool menuActive = false;
    
    ofxLabel graphName;
    ofxLabel graphTypesLabel;
    ofxLabel prefLabel;
    ofxPanel minimumGui;
    ofxToggle expandButton;
    ofxPanel expandedGui;
    ofxIntSlider historySizeSlider;
    ofxToggle splitArrayIntoGraphs;
    
    bool oldTypeToggles[NUMVIEWTYPES];
    ofxToggle viewTypes[NUMVIEWTYPES];
};

#endif /* RapidVisualizer_hpp */
