/**
 * @file    rapidmix.h
 * @author  Michael Zbyszynski
 * @date    12 Jan 2017
 *
 * @copyright
 * Copyright © 2017 Goldsmiths. All rights reserved.
 */

/**
 * @mainpage RAPID-MIX API
 *
 * @section Introduction
 *
 * Hello, API ! <br>
 * All the classes, functions and structs documented here belong to the
 * <code>rapidmix</code> namespace.
 *
 * @section Examples
 * @subsection Simple classification
 * TODO
 */

/////////////////////////////////////////////////////////////////////////////
//  ______            _     _       ___  ____         ___  ______ _____    //
//  | ___ \          (_)   | |      |  \/  (_)       / _ \ | ___ \_   _|   //
//  | |_/ /__ _ _ __  _  __| |______| .  . |___  __ / /_\ \| |_/ / | |     //
//  |    // _` | '_ \| |/ _` |______| |\/| | \ \/ / |  _  ||  __/  | |     //
//  | |\ \ (_| | |_) | | (_| |      | |  | | |>  <  | | | || |    _| |_    //
//  \_| \_\__,_| .__/|_|\__,_|      \_|  |_/_/_/\_\ \_| |_/\_|    \___/    //
//             | |                                                         //
//             |_|                                                         //
/////////////////////////////////////////////////////////////////////////////

#ifndef rapidmix_h
#define rapidmix_h

#define RAPIDMIX_VERSION_MAJOR 2
#define RAPIDMIX_VERSION_MINOR 3
#define RAPIDMIX_VERSION_PATCH 0
#define RAPIDMIX_VERSION "2.3.1"
#define RAPIDMIX_JSON_DOC_VERSION "1.0.0"
#define RAPIDMIX_REVISION "2-MAY-2018"

#include "machineLearning/machineLearning.h"
#include "machineLearning/trainingData.h"
#include "signalProcessing/signalProcessing.h"

#endif
