#include <cassert>
#include <vector>
#include <iostream>
#include "rapidmix.h"

int main() {

    ///////////////////////////////////Test GVF

    rapidmix::gvfTemporalVariation gvf;
    rapidmix::trainingData myGVFData;

    // Record first gesture
    myGVFData.startRecording();
    myGVFData.addElement({ 4.0, 0.7 });
    myGVFData.addElement({ 3.0, 0.8 });
    myGVFData.addElement({ 2.0, 0.9 });
    myGVFData.addElement({ 1.0, 1.0 });
    myGVFData.addElement({ 0.4, 1.2 });
    myGVFData.addElement({ 0.2, 1.4 });
    myGVFData.stopRecording();

    // Record reverse of first gesture
    myGVFData.startRecording();
    myGVFData.addElement({ 0.2, 1.4 });
    myGVFData.addElement({ 0.4, 1.2 });
    myGVFData.addElement({ 1.0, 1.0 });
    myGVFData.addElement({ 2.0, 0.9 });
    myGVFData.addElement({ 3.0, 0.8 });
    myGVFData.addElement({ 4.0, 0.7 });
    myGVFData.stopRecording();

    // Train
    std::cout << "gvf train = " << gvf.train(myGVFData) << std::endl;

    std::cout << "gvf passed." << std::endl;

    //////////////////////////////////////Test RapidLib

    rapidmix::staticRegression myNN;
    rapidmix::trainingData myRLData;

    std::vector<double> input = { 0.2, 0.7 };
    std::vector<double> output = { 3.0 };
    myRLData.recordSingleElement("label", input, output);

    input = { 2.0, 44.2 };
    output = { 20.14 };
    myRLData.recordSingleElement("label", input, output);

    std::cout << "staticRegression train = " << myNN.train(myRLData) << std::endl;

    std::vector<double> inputVec = { 1.618, 18.9 };
    assert(myNN.run(inputVec)[0] == 12.596715279688549);
    std::cout << "staticRegression passed." << std::endl;

    //////////////////////////////////////Test XMM

    rapidmix::xmmConfig xcfg;
    xcfg.relativeRegularization = 0.1;

    rapidmix::trainingData myXmmData;
    std::vector<double> myXmmInput;
    std::vector<double> myXmmOutput;

    myXmmData.startRecording("lab1");
    myXmmInput = { 0.2, 0.7 };
    myXmmData.addElement(myXmmInput, myXmmOutput);
    myXmmData.stopRecording();

    myXmmData.startRecording("lab2");
    myXmmInput = { 0.8, 0.1 };
    myXmmData.addElement(myXmmInput, myXmmOutput);
    myXmmData.stopRecording();

    myXmmData.writeJSON("/var/tmp/testTrainingData.json");

    rapidmix::xmmStaticClassification myGmm(xcfg);

    myGmm.train(myXmmData);

    std::string filepath = "/var/tmp/modelSetDescription";
    myGmm.writeJSON(filepath);

    myXmmInput = { 0.2, 0.7 };

    rapidmix::xmmStaticClassification myGmmFromFile;
    myGmmFromFile.readJSON(filepath);

    assert(myGmm.run(myXmmInput)[0] == myGmmFromFile.run(myXmmInput)[0]);
    std::cout << "xmm passed." << std::endl;

    /////////////////////////////////////Test pipo signal processing

    rapidmix::signalProcessingHost spHost;

    spHost.setGraph("slice<fft:moments,moments>");
    spHost.setAttr("slice.size", 128);
    spHost.setAttr("slice.hop", 64);

    rapidmix::signalAttributes sa;

    spHost.setInputStreamAttributes(sa);

    for (unsigned int i = 0; i < 256; ++i)
    {
        std::vector<float> f({ 1, 2, 3 });
        spHost.frames(0, 1, &f[0], 1, 1);
    }
    std::cout << "pipo passed." << std::endl;

    /////////////////////////////////////Test rapidStream signal processing

    rapidmix::rapidStream myProcessor(5); //create a processor with a window size of 5
    myProcessor.pushToWindow(0.246);
    myProcessor.pushToWindow(0.44);
    myProcessor.pushToWindow(-0.228);
    myProcessor.pushToWindow(0.402);
    myProcessor.pushToWindow(-0.382);

    assert(myProcessor.maximum() == 0.44);
    assert(myProcessor.minimum() == -0.382);
    assert(myProcessor.sum() == 0.478);
    assert(myProcessor.mean() == 0.09559999999999999);
    assert(myProcessor.standardDeviation() == 0.33702557766436664);
    assert(myProcessor.rms() == 0.3503221374677884);

    std::cout << "rapidStream passed." << std::endl;

    /////////////////////////////////////Test Maxi signal processing

    rapidmix::FFT myFFT;
    myFFT.setup(1024, 512, 265);

    for (int i = 0; i < 1024; ++i) {
        float input = (i % 256/256.0);
        myFFT.process(input);
    }

    assert(myFFT.spectralCentroid() == 3520.84277f);
    std::cout << "maxi fft passed." << std::endl;

    return 0;
}
