//
//  LineGraphHistory.hpp
//  RapidVisualizerOSC
//
//  Created by James on 10/11/2017.
//
//

#ifndef LineGraphHistory_h
#define LineGraphHistory_h

#include <stdio.h>
#include "HistoryGraph.hpp"

class LineGraphHistory : public HistoryGraph
{
public:
    LineGraphHistory ( GraphState* state ) : HistoryGraph (state)
    {
        //
    }
    ~LineGraphHistory ( void )
    {
        //
    }
    void updateRep ( void )
    {
        //
    }
    void drawSubGraph ( std::string subLabel, DataContainer<std::list<double>>& data, ofRectangle subLabelRect )
    {
        double
        minIn = 0,
        minOut = 0,
        maxOut = -subLabelRect.height,
        startPosY = subLabelRect.height,
        pointDistance = subLabelRect.width/data.labelData.size(),
        separation = pointDistance/2;
        //halfSeparation = separation/2;
        bool drawZeroSep = false;
        
        if (data.minValue < 0)
        { // Add negative part
            startPosY = subLabelRect.height/2;
            minIn = -data.maxValue;
            minOut = subLabelRect.height/2;
            maxOut /= 2;
            drawZeroSep = true;
        }
        
        ofBeginShape();
        ofFill();
        ofVertex(subLabelRect.x, subLabelRect.y + startPosY);
        double output = mapVals(data.labelData.front(), minIn, data.maxValue, minOut, maxOut );
        ofVertex(subLabelRect.x, subLabelRect.y + startPosY + output);
        uint32_t i = 0;
        for (double d : data.labelData)
        {
            output = mapVals(d, minIn, data.maxValue, minOut, maxOut );
            ofSetColor (graphColor);
            
            ofVertex(subLabelRect.x + pointDistance * i + separation, subLabelRect.y + startPosY + output);
            //ofDrawRectangle(subLabelRect.x + barSize * i + halfSeparation, subLabelRect.y + startPosY, barSize - separation, output );
            ++i;
            
        }
        output = mapVals(data.labelData.back(), minIn, data.maxValue, minOut, maxOut );
        ofVertex(subLabelRect.x + subLabelRect.width, subLabelRect.y + startPosY + output);
        ofVertex(subLabelRect.x + subLabelRect.width, subLabelRect.y + startPosY);
        ofEndShape();
        
        if (drawZeroSep)
        {
            ofSetLineWidth(1.25);
            ofSetColor (175,150,150);
            ofDrawLine(subLabelRect.x, subLabelRect.y + startPosY,
                       subLabelRect.x + subLabelRect.width, subLabelRect.y + startPosY);
        }
    }
    void update ( void )
    {
        //
    }
};


#endif /* LineGraphHistory_h */
