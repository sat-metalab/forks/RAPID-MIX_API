#include "rapidmix.h"
#include "rapidPiPoHost.h"
#include <iostream>
#include <fstream>

//=========================== H O S T === U T I L S ==========================//

static void fromPiPoStreamAttributes(PiPoStreamAttributes &src,
                                     RapidPiPoStreamAttributes &dst)
{
  unsigned int numCols = src.dims[0];
  unsigned int numLabels = src.numLabels;

  if (numLabels > PIPO_MAX_LABELS) {
    numLabels = PIPO_MAX_LABELS;
  }

  if (numLabels > numCols) {
    numLabels = numCols;
  }

  dst.hasTimeTags = src.hasTimeTags;

  if (src.rate <= MIN_PIPO_SAMPLERATE) {
    dst.rate = MIN_PIPO_SAMPLERATE;
  } else if (src.rate >= MAX_PIPO_SAMPLERATE) {
    dst.rate = MAX_PIPO_SAMPLERATE;
  } else {
    dst.rate = src.rate;
  }

  dst.rate = src.rate;
  dst.offset = src.offset;
  dst.width = src.dims[0];
  dst.height = src.dims[1];

  dst.labels = std::vector<std::string>();

  for (unsigned int i = 0; i < numLabels; ++i)
  {
    dst.labels.push_back(std::string(src.labels[i]));
  }

  dst.hasVarSize = src.hasVarSize;
  dst.domain = src.domain;
  dst.maxFrames = src.maxFrames;
}


static void toPiPoStreamAttributes(RapidPiPoStreamAttributes &src,
                                   PiPoStreamAttributes &dst)
{
  const char *labs[src.labels.size()];

  for (unsigned int i = 0; i < src.labels.size(); ++i)
  {
    labs[i] = src.labels[i].c_str();
  }

  dst = PiPoStreamAttributes(
    src.hasTimeTags,
    src.rate,
    src.offset,
    src.width,
    src.height,
    &labs[0],
    src.hasVarSize,
    src.domain,
    src.maxFrames
  );
}

//========================= H O S T === M E T H O D S ========================//

int
RapidPiPoHost::setInputStreamAttributes(RapidPiPoStreamAttributes &rpsa)
{
  PiPoStreamAttributes psa;
  toPiPoStreamAttributes(rpsa, psa);
  return PiPoHost::setInputStreamAttributes(psa);
}

void
RapidPiPoHost::onNewFrame(double time, double weight, PiPoValue *values, unsigned int size)
{
  std::cout << "I received " << size << " new frame(s)" << std::endl;
}

//----------------------------- JSON FORMATTING ------------------------------//

std::string
RapidPiPoHost::getJSON()
{
  Json::Value result = toJSON();
  return result.toStyledString();
}

void
RapidPiPoHost::writeJSON(const std::string &filepath)
{
  Json::Value root = toJSON();
  std::ofstream jsonOut;
  jsonOut.open (filepath);
  Json::StyledStreamWriter writer;
  writer.write(jsonOut, root);
  jsonOut.close();
}

bool
RapidPiPoHost::putJSON(const std::string &jsonMessage)
{
  Json::Value parsedFromString;
  Json::Reader reader;
  bool parsingSuccessful = reader.parse(jsonMessage, parsedFromString);
  return (parsingSuccessful && fromJSON(parsedFromString));
}

bool
RapidPiPoHost::readJSON(const std::string &filepath)
{
  Json::Value root;
  std::ifstream file(filepath);
  file >> root;
  return fromJSON(root);
}

//-------------------------- PRIVATE HOST METHODS ----------------------------//

Json::Value
RapidPiPoHost::toJSON()
{
  Json::Value root;

  //*
  root["docType"] = "rapid-mix:signal-processing";
  root["docVersion"] = RAPIDMIX_JSON_DOC_VERSION;

  Json::Value target;
  target["name"] = "pipo";
  target["version"] = PIPO_SDK_VERSION;

  root["target"] = target;

  Json::Value pipodata;
  pipodata["description"] = this->graphName;

  Json::Value inputStream;
  inputStream["hasTimeTags"] = inputStreamAttrs.hasTimeTags;
  inputStream["rate"] = inputStreamAttrs.rate;
  inputStream["offset"] = inputStreamAttrs.offset;
  inputStream["width"] = inputStreamAttrs.dims[0];
  inputStream["height"] = inputStreamAttrs.dims[1];
  inputStream["labels"] = inputStreamAttrs.labels;
  inputStream["hasVarSize"] = inputStreamAttrs.hasVarSize;
  inputStream["domain"] = inputStreamAttrs.domain;
  inputStream["maxFrames"] = inputStreamAttrs.maxFrames;

  Json::Value outputStream;
  outputStream["hasTimeTags"] = outputStreamAttrs.hasTimeTags;
  outputStream["rate"] = outputStreamAttrs.rate;
  outputStream["offset"] = outputStreamAttrs.offset;
  outputStream["width"] = outputStreamAttrs.dims[0];
  outputStream["height"] = outputStreamAttrs.dims[1];
  outputStream["labels"] = outputStreamAttrs.labels;
  outputStream["hasVarSize"] = outputStreamAttrs.hasVarSize;
  outputStream["domain"] = outputStreamAttrs.domain;
  outputStream["maxFrames"] = outputStreamAttrs.maxFrames;

  Json::Value streams;
  streams["input"] =  inputStream;
  streams["output"] =  outputStream;

  pipodata["streamAttributes"] = streams;

  Json::Value params;
  int n = this->graph->getNumAttrs();
  params.resize(static_cast<Json::ArrayIndex>(n));

  for (unsigned int i = 0; i < n; ++i)
  {
    Json::Value param;
    PiPo::Attr *a = this->graph->getAttr(i);
    param["name"] = a->getName();
    switch (a->getType())
    {
      case PiPo::Bool:
        param["type"] = "Bool";
        param["value"] = a->getInt(0);
        break;
      case PiPo::Enum:
        param["type"] = "Enum";
        param["value"] = std::string(a->getStr(0));
        break;
      case PiPo::String:
        param["type"] = "String";
        param["value"] = std::string(a->getStr(0));
        break;
      case PiPo::Int:
        param["type"] = "Int";
        param["value"] = Json::Value(Json::arrayValue);
        for (int i = 0; i < a->getSize(); ++i)
        {
          param["value"].append(a->getInt(i));
        }
        break;
      case PiPo::Double:
        param["type"] = "Double";
        param["value"] = Json::Value(Json::arrayValue);
        for (int i = 0; i < a->getSize(); ++i)
        {
          param["value"].append(a->getDbl(i));
        }
        break;
      default:
        std::cout << a->getType() << std::endl;
        break;
    }
    params[i] = param;
  }

  pipodata["parameters"] = params;

  root["payload"] = pipodata;
  //*/

  return root;
}

bool
RapidPiPoHost::fromJSON(Json::Value &jv)
{
  //*
  if (jv["docType"].asString().compare("rapid-mix:signal-processing") == 0 &&
      //jv["docVersion"].asString().compare(RAPIDMIX_JSON_DOC_VERSION) == 0 &&
      jv["target"]["name"].asString().compare("pipo") == 0 &&
      jv["payload"].size() > 0) {

    this->setGraph(jv["payload"]["description"].asString());

    Json::Value params = jv["payload"]["parameters"];
    unsigned int nAttrs = params.size();

    for (unsigned int i = 0; i < nAttrs; ++i)
    {
      std::string type = params[i]["type"].asString();
      const char *name = params[i]["name"].asString().c_str();

      if (type.compare("Bool") == 0)
      {
        this->setAttr(name, params[i]["value"].asInt());
      }
      else if (type.compare("Enum") == 0 || type.compare("String") == 0)
      {
        this->setAttr(name, params[i]["value"].asString());
      }
      else if (type.compare("Int") == 0)
      {
        std::vector<int> values;

        for (int j = 0; j < params[i]["value"].size(); ++j)
        {
          values.push_back(params[i]["value"][j].asInt());
        }

        this->setAttr(name, values);
      }
      else if (type.compare("Double") == 0)
      {
        std::vector<double> values;

        for (int j = 0; j < params[i]["value"].size(); ++j)
        {
          values.push_back(params[i]["value"][j].asDouble());
        }

        this->setAttr(name, values);
      }
    }


    Json::Value inputStream = jv["pipodata"]["streamAttributes"]["input"];
    // setInputStreamAttributes(
    //   inputStream["hasTimeTags"].getDbl()//,
    //   //...
    // );
    return true;
  }
  //*/

  return false;
}
