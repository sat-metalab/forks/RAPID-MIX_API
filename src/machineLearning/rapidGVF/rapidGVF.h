/**
 * @file rapidGVF.h
 * Created by Francisco on 04/05/2017.
 * Copyright © 2017 Goldsmiths. All rights reserved.
 *
 * @ingroup machinelearning
 */

#ifndef rapidGVF_h
#define rapidGVF_h

#include <vector>
#include <string>
#include "GVF.h"

namespace rapidmix { class trainingData; }

/**
 * @brief This class is an adapter for the GVF library from Baptiste Caramiaux
 */
class rapidGVF {

public:
    rapidGVF();
    ~rapidGVF();

    bool train(const rapidmix::trainingData &newTrainingData);
    std::vector<double> run(const std::vector<double> &inputVector);
    //TODO: needs a "reset" message

    const std::vector<float> getLikelihoods();
    const std::vector<float> getAlignments();
    const std::vector<std::vector<float> > * getDynamics();
    const std::vector<std::vector<float> > * getScalings();
    const std::vector<std::vector<float> > * getRotations();
    
private:
    GVF gvf;
    GVFGesture currentGesture;
    GVFOutcomes outcomes;
};

#endif
